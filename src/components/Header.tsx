import React from "react";
import { useEffect, useRef } from "react";
import styled from "styled-components";
import Typed from "typed.js";

const StyledHeader = styled.p`
  color: palevioletred;
  font-weight: bold;
  font-style: italic;
  font-size: 2.2em;
  margin-top: 0;
  margin-bottom: 2em;

  white-space: nowrap;

  @media screen and (max-width: 600px) {
    margin-top: 1em;
  }
`;
export default (props: { text: String; onTypedComplete: () => void }) => {
  const { text, onTypedComplete } = props;
  const elem = useRef(null);

  useEffect(() => {
    const typed = new Typed(elem.current, {
      strings: [text],
      typeSpeed: 80,
      cursorChar: "▁",
      onComplete: onTypedComplete,
    });

    return () => {
      typed.destroy();
    };
  }, []);

  return (
    <StyledHeader>
      &gt; <span ref={elem}></span>
    </StyledHeader>
  );
};
