import { CaretLeft } from "phosphor-react";
import React, { useState } from "react";
import {
  Dispatch,
  ReactElement,
  SetStateAction,
  useEffect,
  useRef,
} from "react";
import styled from "styled-components";

const OPEN_ANIMATION_TRANSITION_TIME = "0.8s";

const AccordionItemHeaderStyled = styled.div`
  display: inline-flex;
  width: inherit;

  font-weight: bold;
  font-size: 1.5em;

  &:hover {
    transition: 0.3s;
    color: palevioletred;
  }

  @media screen and (max-width: 600px) {
    width: 100%;
  }
`;

const AccordionItemTitleStyled = styled.div`
  cursor: pointer;
`;

const AccordionItemIconStyled = styled.div<{
  isHover: boolean;
  isOpen: boolean;
}>`
  display: inline-block;
  flex: 1;
  margin-left: auto;
  transition: ${OPEN_ANIMATION_TRANSITION_TIME} ease;

  ${(props) => {
    if (props.isOpen) {
      return `
        flex: 0;
        transition: margin-left,flex ${OPEN_ANIMATION_TRANSITION_TIME} cubic-bezier(.55,.09,.68,.53);
      `;
    }
    if (props.isHover) {
      return `
        transform: translateX(0);
      `;
    }
    return `
      transform: translateX(0.8em);
    `;
  }};
`;

const CaretLeftStyled = styled(CaretLeft)<{ isOpen: boolean }>`
  transform: ${(props) => props.isOpen && "rotate(-90deg)"};
  transition: all ${OPEN_ANIMATION_TRANSITION_TIME}
    cubic-bezier(0.55, 0.09, 0.68, 0.53);
`;

const AccordionItemContentStyled = styled.div<{ isOpen: boolean }>`
  z-index: -1;
  position: relative;

  ${({ isOpen }) =>
    isOpen
      ? `
    opacity: 1;
    max-height: 100rem;
    transform: translateY(0);
    transition: opacity 0.3s ease-out 0.2s, transform 0.6s;
  `
      : `
    visibility: hidden; /* Stop ppl from clicking links */
    opacity: 0;
    max-height: 0; /* For allowing AccordionItemStyled to change cumulative heights */
    transform: translateY(-30px);
    transition: opacity 1s cubic-bezier(.08,.82,.17,1), transform 0.5s;
  `}
`;

const AccordionItemStyled = styled.div<{ translateY: number }>`
  white-space: break-spaces;
  transform: translateY(${({ translateY }) => translateY}px);
  transition: all 0.5s cubic-bezier(0.17, 0.84, 0.44, 1);
  position: absolute;
  max-height: 100rem;
  width: inherit;
`;

interface AccordionHeights {
  cumulativeHeight: number;
  height: number;
}

const AccordionItem = (props: {
  itemNum: number;
  title: String;
  content: ReactElement;
  setAccordionItemHeights: Dispatch<SetStateAction<AccordionHeights[]>>;
  allAccordionItemHeights: AccordionHeights[];
}) => {
  const {
    itemNum,
    title,
    content,
    setAccordionItemHeights: setAccordionItemHeights,
    allAccordionItemHeights: allAccordionItemHeights,
  } = props;
  const ref = useRef(null);

  const [isOpen, setOpen] = useState(false);
  const toggleOpen = () => setOpen((isOpen) => !isOpen);

  const [isHover, setHover] = useState(false);

  useEffect(() => {
    // Alter the height value of this sibling
    const newContent = [...allAccordionItemHeights];
    newContent[itemNum].height = ref?.current.clientHeight ?? 0;

    let cumulativeHeight = 0;
    for (const content of newContent) {
      cumulativeHeight += content.height;
      content.cumulativeHeight = cumulativeHeight;
    }

    setAccordionItemHeights(newContent);
  }, [isOpen]);

  return (
    <AccordionItemStyled
      translateY={allAccordionItemHeights[itemNum - 1]?.cumulativeHeight ?? 0}
      ref={ref}
    >
      <AccordionItemHeaderStyled>
        <AccordionItemTitleStyled
          onClick={toggleOpen}
          onMouseEnter={() => setHover(true)}
          onMouseOut={() => setHover(false)}
        >
          {title}
        </AccordionItemTitleStyled>
        <AccordionItemIconStyled isHover={isHover} isOpen={isOpen}>
          <CaretLeftStyled isOpen={isOpen} />
        </AccordionItemIconStyled>
      </AccordionItemHeaderStyled>
      <AccordionItemContentStyled isOpen={isOpen}>
        {content}
      </AccordionItemContentStyled>
    </AccordionItemStyled>
  );
};

export default AccordionItem;
