import React, { useState } from "react";
import AccordionItem from "./AccordionItem";
import { AccordionContentInterface } from "../content";
import styled from "styled-components";

const AccordionStyled = styled.div<{ isHidden: boolean }>`
  opacity: ${({ isHidden }) => (isHidden ? "0" : "1")};
  transform: translateX(${({ isHidden }) => (isHidden ? "2rem" : "0")});
  transition: ease-in 0.4s;
  width: 30em;

  @media screen and (max-width: 600px) {
    width: 100%;
  }
`;

const Accordion = ({
  content,
  isHidden,
}: {
  content: Array<AccordionContentInterface>;
  isHidden: boolean;
}) => {
  const [accordionItemHeights, setAccordionItemHeights] = useState(
    Array.from({ length: content.length }, () => ({
      cumulativeHeight: 0,
      height: 0,
    }))
  );

  return (
    <AccordionStyled isHidden={isHidden}>
      {content.map(({ title, content }, itemNum) => (
        <AccordionItem
          itemNum={itemNum}
          title={title}
          content={content}
          setAccordionItemHeights={setAccordionItemHeights}
          allAccordionItemHeights={accordionItemHeights}
        />
      ))}
    </AccordionStyled>
  );
};

export default Accordion;
